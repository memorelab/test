## Case

I. Existe hoje um sistema de login e perfis de usuários com 10 milhões de usuários (10*10^6).
Uma lentidão na tela inicial do sistema, é feita uma consulta ao BD para pegar as informações do usuário e exibi-las de forma personalizada.
Picos de logins simultâneos impactam muito na resposta do sistema.

Que tipo de melhoria poderia ser feita ?


II. Codificar um novo sistema de login para muitos usuários simultâneos e carregamento da tela inicial.
Sistema web com contéudos estático e dinâmico. Na empresa existe um outro sistema que também requisitará os dados do usuário.

## Stack técnica

* Java 8.
* Spring (spring boot).

## Resposta 1

### Arquitetural

#### Premissas

 * Autenticação e Autorização inicado por Usuário/senha recuperando lista de Roles (profiles).
* APP transformada em “API First” com frontend desacoplado (MVVC).

#### Autenticação independente

Para implementar um sistema de Login que será usado por outros sistemas sugestão é desacoplar o sistema de autenticação usando um protocolo conhecido como Oauth2, JWT entre outros.

Além de proprocionar uma forma unificada de Autenticação e Autorização fica fácil de optimizar a perfomance com algumas técnicas:

Cache, tirar a consulta direta ao Banco de dados implementando uma camada intermédiaria de acesso mais rápido como Memcache, Redis etc.

Partition, separar o usuário por grupos, por exemplo que começam com a letra A ou da cidade de São Paulo.

Sharding.



##### Modo 1: Api Gateway

![Diagrama_API_Gateway](docs/images/api_gw.jpg) 

API Gateway funciona como autorizador e autenticador para as aplicações e seus resources funcionando como um “frontend” das chamadas, sendo assim, em muitos casos deixa o fluxo de autenticação e autorização transparente nas chamadas entre diferentes resources.

Também implementa outras funcionalidades quando pensamos em “API First” como Rate limit, portal de documentação etc.


##### Mode 2: Auth Server

![Diagrama_Auth_Server](docs/images/auth_server.jpg) 

No mesmo modelo de autenticação e autorização mas sem ser o “frontend” das chamadas.

#### Resposta 2

Olhando para visão da implementação seguindo arquitetura definida na resposta 1, no exemplo usarei os componentes sugeridos como Java 8 e Spring Boot.

Seguindo a mesma gama de componentes o modelo de auth/autho são implementados usando:

* Spring MVC.
* Spring Security OAuth2.

O Spring implementou via Servlet Filters e @Annotations as configuração de permissão de acesso a cada resource (method) implementado no Spring MVC, sendo assim, todo o ciclo de vida de authentication/authorization é feita de forma transparente ao desenvolvedor que se preocupa apenas com a correta configuração de PROFILE(ROLE) de acesso.

Exemplo pode-se ver nesse bloco de código, associando ROLE "User" aos resources.

```
#!java

  @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests().antMatchers("/login").permitAll().and()
        // default protection for all resources (including /oauth/authorize)
            .authorizeRequests()
                .anyRequest().hasRole("USER")
        // ... more configuration, e.g. for form login
    }
```

Exemplo completo:
[Projeto Spring Oauth2 - Samples Tonr](spring-security-oauth/samples/oauth2/tonr)


#### Perguntas

Relacionados ao processo de autenticação:

* Quais os valores de autenticação distribuidos por dia, semana e mês ?

* O quê para negócio é considerado aceitável como tempo de resposta ?

* Qual é o tempo médio de um cadastro de novo usuário (ciclo de vida para cache) ?

* Qual expectativa de crescimento de usuários para próximos 3, 6 meses e 1 ano ?


Referências:

https://spring.io/guides/tutorials/spring-boot-oauth2/

https://getkong.org/about/faq/

https://www.oauth.com/

https://github.com/spring-projects/spring-security-oauth